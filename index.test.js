const index = require('./index');

describe('Main file startup', () => {
	test('main() should be called', () => {
		expect(index.main()).toBeTruthy();
	});
});
