//
// Main index.js file which starts up the nPICK client.
// 

const main = () => {
	console.log('nPick V1.0');
	return 'nPICK V1.0';
}
main();

module.exports = {
	main: main
};
